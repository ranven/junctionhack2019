import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import { ContentfulService } from '../../core/contentful.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-park-activities-view',
	templateUrl: './park-activities-view.component.html',
	styleUrls: ['./park-activities-view.component.scss'],
})
export class ParkActivitiesViewComponent implements OnInit {
	public park: Promise<Entry<Park>>;
	public activities: Promise<any[]>;
	public locationUrl = location.href;

	constructor(
		private contentful: ContentfulService,
		private route: ActivatedRoute,
	) {}

	async ngOnInit() {
		this.park = this.contentful.getEntry<Park>(
			this.route.snapshot.params.parkId,
		);

		const park = await this.park;

		this.activities = this.contentful
			.query<any>({
				content_type: 'service',
				'fields.park.sys.id': park.sys.id,
			})
			.then(res => {
				console.log(res.items);
				return res.items;
			});
	}
}
