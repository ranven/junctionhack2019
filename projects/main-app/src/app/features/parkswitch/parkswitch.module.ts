import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParkswitchComponent } from './parkswitch.component';
import { RouterModule } from '@angular/router';

@NgModule({
	declarations: [ParkswitchComponent],
	imports: [CommonModule, RouterModule],
	exports: [ParkswitchComponent],
})
export class ParkswitchModule {}
