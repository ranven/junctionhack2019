import * as csvtojson from 'csvtojson';

interface CounterData {
  "CounterReadingID": string;
  "CounterID_ASTA": string;
  "SequenceNumber": string;
  "StartTime": string;
  "EndTime": string;
  "Visits": string;
  "ASTA_Counters": {
    "CounterID_PAVE": string;
    "Name_ASTA": string;
    "InstallationDate": string;
    "NationalParkCode": string;
    "Municipality": string;
    "RegionalUnit": string;
    "RegionalEntity": string;
  },
  "PAVE_Counters": {
    "Globalid": string;
    "Name": string;
    "Manager": string;
    "AdditionalInfo": string;
    "CoordinateNorth": string;
    "CoordinateEast": string;
  }
}

export async function parseTheSuperFile() {
  console.log('GET data/theFileForYeet.csv');

  return csvtojson()
    .fromFile('data/theFileForYeet.csv', { encoding: 'utf-8' }).then((data: CounterData[]) => {
      const set = new Set();
      data.forEach(obj => {
        set.add(`${obj.ASTA_Counters.NationalParkCode}: ${obj.PAVE_Counters.Name}`)
      })
      return [...set].sort()
      return set
    })
}

// pohjois länsikulma   60.3604226,24.3990603
// etelä itäkulma       60.1984466,24.7278053