import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParkActivitiesViewComponent } from './park-activities-view.component';

const routes: Routes = [{ path: '', component: ParkActivitiesViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParkActivitiesViewRoutingModule { }
