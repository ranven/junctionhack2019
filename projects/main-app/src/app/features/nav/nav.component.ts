import { Component, OnInit, Input } from '@angular/core';
import { Entry } from 'contentful';

@Component({
	selector: 'app-nav',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
	@Input()
	park: Entry<Park>;

	constructor() {}

	ngOnInit() {}
}
