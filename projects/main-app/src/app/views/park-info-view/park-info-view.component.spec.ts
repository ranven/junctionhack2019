import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkInfoViewComponent } from './park-info-view.component';

describe('ParkInfoViewComponent', () => {
  let component: ParkInfoViewComponent;
  let fixture: ComponentFixture<ParkInfoViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkInfoViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkInfoViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
