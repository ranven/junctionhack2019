import { Component, OnInit, Input } from '@angular/core';
import { Entry } from 'contentful';

@Component({
	selector: 'app-parkswitch',
	templateUrl: './parkswitch.component.html',
	styleUrls: ['./parkswitch.component.scss'],
})
export class ParkswitchComponent implements OnInit {
	@Input()
	park: Entry<Park>;

	constructor() {}

	ngOnInit() {
		console.log(this.park);
	}
}
