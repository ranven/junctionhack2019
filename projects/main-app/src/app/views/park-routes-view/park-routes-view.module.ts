import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParkRoutesViewRoutingModule } from './park-routes-view-routing.module';
import { ParkRoutesViewComponent } from './park-routes-view.component';
import { ParkswitchModule } from '../../features/parkswitch/parkswitch.module';
import { NavModule } from '../../features/nav/nav.module';

@NgModule({
	declarations: [ParkRoutesViewComponent],
	imports: [
		CommonModule,
		ParkRoutesViewRoutingModule,
		ParkswitchModule,
		NavModule,
	],
})
export class ParkRoutesViewModule {}
