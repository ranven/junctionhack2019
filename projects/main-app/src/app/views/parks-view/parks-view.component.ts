import { Component, OnInit } from '@angular/core';
import { EntryCollection, Entry } from 'contentful';
import { ContentfulService } from '../../core/contentful.service';

@Component({
	selector: 'app-parks-view',
	templateUrl: './parks-view.component.html',
	styleUrls: ['./parks-view.component.scss'],
})
export class ParksViewComponent implements OnInit {
	public parks: Promise<Entry<Park>[]>;

	constructor(private contentful: ContentfulService) {}

	ngOnInit() {
		this.parks = this.contentful
			.query<Park>({ content_type: 'park' })
			.then(res => {
				console.log(res.items);
				return res.items;
			});
	}
}
