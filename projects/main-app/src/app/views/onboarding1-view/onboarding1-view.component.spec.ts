import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Onboarding1ViewComponent } from './onboarding1-view.component';

describe('Onboarding1ViewComponent', () => {
  let component: Onboarding1ViewComponent;
  let fixture: ComponentFixture<Onboarding1ViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Onboarding1ViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Onboarding1ViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
