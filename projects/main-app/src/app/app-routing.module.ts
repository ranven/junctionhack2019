import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		loadChildren: () =>
			import('./views/parks-view/parks-view.module').then(
				m => m.ParksViewModule,
			),
	},
	{
		path: 'park/:parkId',
		pathMatch: 'full',
		loadChildren: () =>
			import('./views/park-info-view/park-info-view.module').then(
				m => m.ParkInfoViewModule,
			),
	},
	{
		path: 'park/:parkId/routes',
		loadChildren: () =>
			import('./views/park-routes-view/park-routes-view.module').then(
				m => m.ParkRoutesViewModule,
			),
	},
	{
		path: 'o1',
		loadChildren: () =>
			import('./views/onboarding1-view/onboarding1-view.module').then(
				m => m.Onboarding1ViewModule,
			),
	},
	{
		path: 'park/:parkId/quests',
		loadChildren: () =>
			import('./views/park-quests-view/park-quests-view.module').then(
				m => m.ParkQuestsViewModule,
			),
	},
	{
		path: 'map/:routeId',
		loadChildren: () =>
			import('./views/map-view/map-view.module').then(m => m.MapViewModule),
	},
	{
		path: 'park/:parkId/activities',
		loadChildren: () =>
			import('./views/park-activities-view/park-activities-view.module').then(
				m => m.ParkActivitiesViewModule,
			),
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
