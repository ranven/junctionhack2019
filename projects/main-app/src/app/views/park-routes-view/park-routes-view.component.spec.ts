import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkRoutesViewComponent } from './park-routes-view.component';

describe('ParkRoutesViewComponent', () => {
  let component: ParkRoutesViewComponent;
  let fixture: ComponentFixture<ParkRoutesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkRoutesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkRoutesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
