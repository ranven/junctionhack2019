import * as shapefile from 'shapefile';
import { Feature, Geometry } from 'geojson';

type Route = Feature<
	Geometry,
	{
		[name: string]: any;
	}
>;

export async function parseShapefile(query: string) {
	if (!query) {
		return 'must include query';
	}
	return shapefile
		.open('./data/lipas_kaikki_reitit.shp', './data/lipas_kaikki_reitit.dbf', {
			encoding: '',
		})
		.then(async src => {
			const results: Feature<
				Geometry,
				{
					[name: string]: any;
				}
			>[] = [];

			const read = (result: {
				done: boolean;
				value: Feature<
					Geometry,
					{
						[name: string]: any;
					}
				>;
			}) => {
				if (result.done) return results;
				const nimi: string = result.value.properties.nimi_fi;
				if (!nimi) {
					return false;
				}
				const quryOk = nimi.toLowerCase().includes(query.toLowerCase());
				if (quryOk) {
					results.push(result.value);
				}
				return src.read().then(read);
			};

			await src.read().then(read);

			results.filter(result => {
				const nimi: string = result.properties.nimi_fi;
				if (!nimi) {
					return false;
				}
				return nimi.toLowerCase().includes(query.toLowerCase());
			});

			return results;
		});
}

// pohjois länsikulma   60.3604226,24.3990603
// etelä itäkulma       60.1984466,24.7278053
