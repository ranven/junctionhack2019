import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkswitchComponent } from './parkswitch.component';

describe('ParkswitchComponent', () => {
  let component: ParkswitchComponent;
  let fixture: ComponentFixture<ParkswitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkswitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkswitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
