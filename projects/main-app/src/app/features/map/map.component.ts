import { Component, OnInit, AfterViewInit } from '@angular/core';
import mapboxgl from 'mapbox-gl';
import { taiskierrosdata } from './taivaskierros';
@Component({
	selector: 'app-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit {
	private map: mapboxgl.Map;

	constructor() {}

	ngAfterViewInit() {
		mapboxgl.accessToken =
			'pk.eyJ1IjoidGVlbXVsYWhqYWxhaHRpIiwiYSI6ImNqM29ya3oyejAwMnEzM3FkNzBhMnQ5dXAifQ.4g-UQN15VUh36maNSEAK3Q';

		this.map = new mapboxgl.Map({
			container: 'map',
			style: 'mapbox://styles/teemulahjalahti/ck30i8knb0yyt1clmj0r0jprc',
			center: [24.07539, 68.05678],
			zoom: 11,
		});

		this.map.on('load', () => {
			this.map.addLayer({
				id: 'route',
				type: 'line',
				source: {
					type: 'geojson',
					data: taiskierrosdata,
				},
				layout: {
					'line-join': 'round',
					'line-cap': 'round',
				},
				paint: {
					'line-color': 'red',
					'line-width': 8,
				},
			});
		});
	}
}
