import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParkQuestsViewRoutingModule } from './park-quests-view-routing.module';
import { ParkQuestsViewComponent } from './park-quests-view.component';
import { NavModule } from '../../features/nav/nav.module';
import { ParkswitchModule } from '../../features/parkswitch/parkswitch.module';

@NgModule({
	declarations: [ParkQuestsViewComponent],
	imports: [
		CommonModule,
		ParkQuestsViewRoutingModule,
		ParkswitchModule,
		NavModule,
	],
})
export class ParkQuestsViewModule {}
