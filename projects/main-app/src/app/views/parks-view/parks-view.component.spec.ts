import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParksViewComponent } from './parks-view.component';

describe('ParksViewComponent', () => {
  let component: ParksViewComponent;
  let fixture: ComponentFixture<ParksViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParksViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParksViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
