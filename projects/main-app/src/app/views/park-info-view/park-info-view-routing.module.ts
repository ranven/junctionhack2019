import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParkInfoViewComponent } from './park-info-view.component';

const routes: Routes = [{ path: '', component: ParkInfoViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParkInfoViewRoutingModule { }
