import { Component, OnInit } from '@angular/core';
import { Entry, EntryCollection } from 'contentful';
import { ContentfulService } from '../../core/contentful.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-park-quests-view',
	templateUrl: './park-quests-view.component.html',
	styleUrls: ['./park-quests-view.component.scss'],
})
export class ParkQuestsViewComponent implements OnInit {
	public park: Promise<Entry<Park>>;
	public quests: Promise<any[]>;

	public locationUrl = location.href;

	constructor(
		private contentful: ContentfulService,
		private route: ActivatedRoute,
	) {}

	ngOnInit() {
		this.park = this.contentful.getEntry<Park>(
			this.route.snapshot.params.parkId,
		);

		this.quests = this.contentful
			.query<any>({ content_type: 'quest' })
			.then(res => {
				console.log(res.items);
				return res.items;
			});
	}
}
