import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Onboarding1ViewComponent } from './onboarding1-view.component';

const routes: Routes = [{ path: '', component: Onboarding1ViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Onboarding1ViewRoutingModule { }
