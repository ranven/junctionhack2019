import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParkRoutesViewComponent } from './park-routes-view.component';

const routes: Routes = [{ path: '', component: ParkRoutesViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParkRoutesViewRoutingModule { }
