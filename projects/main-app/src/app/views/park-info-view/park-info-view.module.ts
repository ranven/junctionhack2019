import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParkInfoViewRoutingModule } from './park-info-view-routing.module';
import { ParkInfoViewComponent } from './park-info-view.component';
import { MarkdownModule } from 'ngx-markdown';
import { ParkswitchModule } from '../../features/parkswitch/parkswitch.module';
import { NavModule } from '../../features/nav/nav.module';

@NgModule({
	declarations: [ParkInfoViewComponent],
	imports: [
		CommonModule,
		ParkInfoViewRoutingModule,
		MarkdownModule.forRoot(),
		ParkswitchModule,
		NavModule,
	],
})
export class ParkInfoViewModule {}
