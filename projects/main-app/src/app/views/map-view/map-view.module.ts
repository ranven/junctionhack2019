import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapViewRoutingModule } from './map-view-routing.module';
import { MapViewComponent } from './map-view.component';
import { MapModule } from '../../features/map/map.module';

@NgModule({
	declarations: [MapViewComponent],
	imports: [CommonModule, MapViewRoutingModule, MapModule],
})
export class MapViewModule {}
