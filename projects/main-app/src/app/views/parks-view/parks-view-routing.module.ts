import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParksViewComponent } from './parks-view.component';

const routes: Routes = [{ path: '', component: ParksViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParksViewRoutingModule { }
