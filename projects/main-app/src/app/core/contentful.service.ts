import { Injectable } from '@angular/core';
import * as contentful from 'contentful';
import { environment } from '../../environments/environment';
@Injectable({
	providedIn: 'root',
})
export class ContentfulService {
	private client: contentful.ContentfulClientApi;

	constructor() {
		this.init();
	}

	init() {
		const { space, accessToken } = environment;
		this.client = contentful.createClient({
			space,
			accessToken,
			resolveLinks: true,
		});
	}

	getEntry<T>(id: string) {
		return this.client.getEntry<T>(id);
	}

	query<T>(query?: { content_type: string; [key: string]: any }) {
		return this.client.getEntries<T>(query);
	}
}
