import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Onboarding1ViewRoutingModule } from './onboarding1-view-routing.module';
import { Onboarding1ViewComponent } from './onboarding1-view.component';


@NgModule({
  declarations: [Onboarding1ViewComponent],
  imports: [
    CommonModule,
    Onboarding1ViewRoutingModule
  ]
})
export class Onboarding1ViewModule { }
