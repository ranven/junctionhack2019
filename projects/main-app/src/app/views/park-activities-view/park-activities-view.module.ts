import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParkActivitiesViewRoutingModule } from './park-activities-view-routing.module';
import { ParkActivitiesViewComponent } from './park-activities-view.component';
import { ParkswitchModule } from '../../features/parkswitch/parkswitch.module';
import { NavModule } from '../../features/nav/nav.module';

@NgModule({
	declarations: [ParkActivitiesViewComponent],
	imports: [
		CommonModule,
		ParkActivitiesViewRoutingModule,
		ParkswitchModule,
		NavModule,
	],
})
export class ParkActivitiesViewModule {}
