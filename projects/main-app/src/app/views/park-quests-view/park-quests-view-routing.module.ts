import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParkQuestsViewComponent } from './park-quests-view.component';

const routes: Routes = [{ path: '', component: ParkQuestsViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParkQuestsViewRoutingModule { }
