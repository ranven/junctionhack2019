import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import { ContentfulService } from '../../core/contentful.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-park-routes-view',
	templateUrl: './park-routes-view.component.html',
	styleUrls: ['./park-routes-view.component.scss'],
})
export class ParkRoutesViewComponent implements OnInit {
	public park: Promise<Entry<Park>>;
	public routes: Promise<any[]>;

	constructor(
		private contentful: ContentfulService,
		private route: ActivatedRoute,
	) {}

	ngOnInit() {
		this.park = this.contentful.getEntry<Park>(
			this.route.snapshot.params.parkId,
		);
	}
}
