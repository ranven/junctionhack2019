import { Component, OnInit } from '@angular/core';
import { Entry } from 'contentful';
import { ContentfulService } from '../../core/contentful.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-park-info-view',
	templateUrl: './park-info-view.component.html',
	styleUrls: ['./park-info-view.component.scss'],
})
export class ParkInfoViewComponent implements OnInit {
	public park: Promise<Entry<Park>>;

	constructor(
		private contentful: ContentfulService,
		private route: ActivatedRoute,
	) {}

	ngOnInit() {
		this.park = this.contentful.getEntry<Park>(
			this.route.snapshot.params.parkId,
		);
	}
}
