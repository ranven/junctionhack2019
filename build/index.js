"use strict";
exports.__esModule = true;
var express = require("express");
var getRoutes_1 = require("./getRoutes");
var app = express();
var port = 3000;
app.get('/', function (req, res) { return res.send('YEET FROM HERE'); });
app.get('/routes', function (req, res) { return res.send(getRoutes_1.getRoutes()); });
app.listen(port, function () { return console.log("\uD83D\uDEAC R\u00F6\u00F6ki is listening you on port " + port + "!\n\nYEET ME http://localhost:" + port); });
