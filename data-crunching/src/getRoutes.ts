import fetch from 'node-fetch';

interface SportsPlaceId {
  sportsPlaceId: number;
}

export async function getRoutes() {
  const ids: SportsPlaceId[] = await fetch('http://lipas.cc.jyu.fi/api/sports-places').then(res => res.json())

  const routes = await Promise.all(ids.map(({ sportsPlaceId }) => {
    return fetch('http://lipas.cc.jyu.fi/api/sports-places/' + sportsPlaceId).then(res => res.json())
  }))

  return routes;
}
