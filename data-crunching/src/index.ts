import * as express from 'express';
import { getRoutes } from './getRoutes';
import { parseShapefile } from './parseShapefile';
import * as fs from 'fs';
import { parseTheSuperFile } from './parseTheSuperFile';
const app = express();
const port = 3000;

app.use(express.static('data'));
app.get('/', (req, res) => res.send('YEET FROM HERE'));
app.get('/routes', (req, res) => getRoutes().then(data => res.json(data)));
app.get('/shp', (req, res) => {
	parseShapefile(req.query.query).then(data => res.json(data));
});
app.get('/parseTheSuperFile', (req, res) => {
	parseTheSuperFile().then(data => res.json(data));
});

app.listen(port, () =>
	console.log(`🚬 Rööki is listening you on port ${port}!

YEET ME http://localhost:${port}`),
);
