import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkQuestsViewComponent } from './park-quests-view.component';

describe('ParkQuestsViewComponent', () => {
  let component: ParkQuestsViewComponent;
  let fixture: ComponentFixture<ParkQuestsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkQuestsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkQuestsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
