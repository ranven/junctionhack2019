import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkActivitiesViewComponent } from './park-activities-view.component';

describe('ParkActivitiesViewComponent', () => {
  let component: ParkActivitiesViewComponent;
  let fixture: ComponentFixture<ParkActivitiesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkActivitiesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkActivitiesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
