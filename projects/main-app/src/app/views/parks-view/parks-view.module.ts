import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ParksViewRoutingModule } from './parks-view-routing.module';
import { ParksViewComponent } from './parks-view.component';


@NgModule({
  declarations: [ParksViewComponent],
  imports: [
    CommonModule,
    ParksViewRoutingModule
  ]
})
export class ParksViewModule { }
